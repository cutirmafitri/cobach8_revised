swagger: "2.0"
info:
  description: "This is Bakergun Games backend server service users. Demo ReSTAPI on [https://bakergun-backend-service-users.herokuapp.com](https://bakergun-backend-service-users.herokuapp.com) "
  version: "1.0.0"
  title: "Bakergun Backend Service Users"
host: "localhost:8080"
basePath: "/api/v1"
schemes:
- "http"
- "https"
paths:
  /signup:
    post:
      tags:
      - "Register"
      summary: "Add a new user"
      description: "create user game (register)"
      operationId: "registerUserGame"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - in: "body"
        name: "body"
        description: "New user already add on Games"
        required: true
        schema:
          $ref: "#/definitions/SignUp"
      responses:
        "201":
          description: "Success create new user."
        "400":
          description: "Please fill the username, email, and password"
        "500":
          description: "Found error while register"
          schema:
            $ref: "#/definitions/ApiResponse"
            
  /login:
      post:
        tags:
        - "Login"
        summary: "login to existing user"
        description: "login existing user"
        operationId: "loginUserGame"
        consumes:
        - "application/json"
        produces:
        - "application/json"
        parameters:
        - in: "body"
          name: "body"
          description: "Login success"
          required: true
          schema:
            $ref: "#/definitions/Login"
        responses:
          "401":
            description: "Authentication failed. Username not found"
          "400":
            description: "Something error when login"
    
    /getoneUserById:
        post:
          tags:
          - "Get One User By Id"
          summary: "get User By Id"
          description: "get One User"
          operationId: "getOneUserGame"
          consumes:
          - "application/json"
          produces:
          - "application/json"
          parameters:
          - in: "body"
            name: "body"
            description: ""
            required: true
            schema:
              $ref: "#/definitions/getoneUserById"
          responses:
            
      
definitions:
  SignUp:
    type: "object"
    properties:
      username:
        type: "string"
      email:
        type: "string"
      password:
        type: "string"
  ApiResponse:
    type: "object"
    properties: 
      message: 
        type: "string"
  Login:
    type: "object"
    properties:
      messeges:
        type: "string"
            
            